from req import *

ssl._create_default_https_context = ssl._create_unverified_context
br=mechanize.Browser()

br.set_handle_robots(False)
br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]
#br.addheaders = [('User-agent','Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')('Accept','text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',)]
'''br.addheaders = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
   'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
   'Accept-Encoding': 'none',
   'Accept-Language': 'en-US,en;q=0.8',
   'Connection': 'keep-alive'}'''
br.set_handle_refresh(False)

def set_input(value):
	Te.config(state=NORMAL)
	Te.delete(1.0, END)
	Te.insert(END, value)
	Te.config(state=DISABLED)

def rClicker(e):
    ''' right click context menu for all Tk Entry and Text widgets
    '''

    try:
        def rClick_Copy(e, apnd=0):
            e.widget.event_generate('<Control-c>')

        def rClick_Cut(e):
            e.widget.event_generate('<Control-x>')

        def rClick_Paste(e):
            e.widget.event_generate('<Control-v>')

        e.widget.focus()

        nclst = [
					('   Cut', lambda e=e: rClick_Cut(e)),
					('   Copy', lambda e=e: rClick_Copy(e)),
					('   Paste', lambda e=e: rClick_Paste(e)),
                                                               ]

        rmenu = Menu(None, tearoff=0, takefocus=0)

        for (txt, cmd) in nclst:
            rmenu.add_command(label=txt, command=cmd)

        rmenu.tk_popup(e.x_root+40, e.y_root+10,entry="0")

    except TclError:
        print ' - rClick menu, something wrong'
        pass

    return "break"


def rClickbinder(r):

    try:
        for b in [ 'Text', 'Entry', 'Listbox', 'Label']: #
            r.bind_class(b, sequence='<Button-3>',
                         func=rClicker, add='')
    except TclError:
        print ' - rClickbinder, something wrong'
        pass

def cms_f():
	m = True
	site =str(url.get())	
	if site=="":
		set_input("...!!!No Url given!!!...")
	else:
		if ("https://" in site or "http://" in site or "www." in site) and ".com" in site:	
			p = urllib2.urlopen(site)
			p= p.read()
			s = BeautifulSoup(p,'html.parser')
			for l in s.find_all("meta"):
				if l.get("name")=="generator":
					d= l.get("content")
					m =False
					
			if m:
				d= "Cannot find the CMS :("
				
			dom = whois.whois(site)
			set_input(str(dom)+"\n CMS: "+str(d))
		else:
			set_input("                               !!!URL-ERR0R!!!\n  \n                     Try adding 'https://' or 'http://' or 'www' in the url ")

def domain():
	site = str(url.get())
	if site=="":
		set_input("...!!!No Url given!!!...")
	else:
		if ("https://" in site or "http://" in site or "www." in site) and ".com" in site:	
			d = whois.whois(site)
			set_input(str(d))
		else:
			set_input("                               !!!URL-ERR0R!!!\n  \n                     Try adding 'https://' or 'http://' or 'www' in the url ")

def XSScanner1():
	#def paramfind(ur2):
	#	br.open(ur2)
	#	for l in br.links():
	#	    print l.url
	set_input("Checking....")
	 
	site = str(url.get())
	if site=="":
		set_input("...!!!No Url given!!!...")
	else:
		if ("https://" in site or "http://" in site or "www." in site) and ".com" in site:	
			parsed_url = urlparse.urlparse(site)
			parsed_url_list = list(parsed_url)
			parameters = dict(urlparse.parse_qs(parsed_url_list[4]))
			d=[]
			if parameters =={}:   
				
				d.append( "No parameters in the url........." )
				#paramfind(ur1) 
				k="Url has no parameters,use XSScanner2"	
				set_input(str(d))
			else:
				list2 = parsed_url_list[4];
				f=open("list1.txt")
				count=0
				for i in parameters:
					r="qqqqqqqqq"
					print "Checking the "+ i +" parameter:"
					while len(r)>5:
						r=f.readline()
						parameters[i] = r        #changing a single parameter
						parsed_url_list[4] = urlencode(parameters)
						some_url=urlparse.urlunparse(parsed_url_list)
						page=urllib.urlopen(some_url)
						if str(r) in page.read() and len(r)>4:
							count+=1
							d.append(str(r))
							print str(r)+":"+str(count)
					if count>0:
						k="URL is Vulnerable to XSS \n Payloads Succesfull:"
						for i in d:
							k+="\n"+str(i)				          
						set_input(k)
					else:
						k="URL is not Vulnerable to XSS"
						set_input(k)
		else:
			set_input("                               !!!URL-ERR0R!!!\n  \n                     Try adding 'https://' or 'http://' or 'www' in the url ")

def spider():
	site=str(url.get())
	if "https://" in site or "http://" in site or "www." in site:
		URL = site
	elif ".com" in site:

		URL = "https://"+site
	else:
		URL = "https://"+site+".com"
	print URL
	response=br.open(URL)
	Urls=[]
	lines = []
	line = response.readline()
	count=0
	while line != "":
		lines.append(line)
		line = response.readline()
	for line in lines:
		if line.find("href=") != -1:
			line_split = line.split('href="')
			for i in range(len(line_split)):
				line_split[i] = line_split[i][:line_split[i].find('"')]
				if i != 0:
					count+=1
					Urls.append(str(line_split[i])) 
					
	if Urls!=[]:
		k=str(count)+" Urls found:"
		count=1
		for i in Urls:
			if "https://" in i:
				k+="\n"+str(count)+": "+str(i)			
				count+=1
			else:
				k+="\n"+str(count)+": "+str(URL)+str(i)			
				count+=1
			set_input(k)
	else:
		set_input("No Urls found")

"""(old)
def spider():
	site=str(url.get())
	if "https://" in site or "http://" in site or "www." in site:
		URL = site
	elif ".com" in site:

		URL = "https://"+site
	else:
		URL = "https://"+site+".com"
	print URL
	firstDomains = []
	count=0
	br.open(URL) 
	for link in br.links():     
		if URL in str(link.absolute_url):
			count+=1
			firstDomains.append(str(link.absolute_url))
	firstDomains = list(firstDomains)
	if firstDomains != []:
		k=str(count)+" Urls found:"
		count=1
		for i in firstDomains:
			k+="\n"+str(count)+": "+str(i)			
			count+=1
		set_input(k)
	else:
		set_input("No Urls found")
		"""
	
"""	#(old)
def spider():
	X=str(url.get())
	ur1="http://google.co.in/search?q=inurl:"+X+".com"
	response=br.open(ur1)
	k="URLS found:"
	for link in br.links():
		if "https://"+X in link.absolute_url:
			k+="\n"+str(link.url)
			print str(link.url)
	set_input(k)"""

def Scanner2():
	d="nope"
	site = str(url.get())
	if site=="":
		set_input("...!!!No Url given!!!...")
	else:
		if ("https://" in site or "http://" in site or "www." in site) and ".com" in site:	
			response=br.open(site)
			soup=BeautifulSoup(response,'html.parser')
			nlfs=[]
			nlff=[]
			for a in soup.find_all("form"):
				if (a.get("name")!=None):
					nlff.append(str(a.get("name")))
			for b in soup.find_all("input"):
				if (b.get("name")!=None) and (b.get("type")=="text" or b.get("type")==None)and b.get("name")not in nlff:
					nlfs.append(str(b.get("name")))
			for c in soup.find_all("textarea"):
				if (c.get("name")!=None) and (c.get("type")!="hidden")and c.get("name")not in nlff and c.get("name")not in nlfs:
					nlfs.append(str(c.get("name")))
			
			if nlfs==[] and nlff==[]:
				set_input("No text fields to inject XSS Payloads")
			else:
				for na in nlfs:
					print na
					f=open("list1.txt")
					while True:
						r=f.readline()
						print "Injecting the below payload: "
						print r
						driver = webdriver.Firefox()
						driver.get(site)
						time.sleep(10)
						try: 
							element = driver.find_element_by_name(na)
							element.send_keys(r)
							element.submit()
						except():
							d= "Parameter not tracable"
						driver.implicitly_wait(20)
						x = raw_input("Do u want to inject another payload??[y/n]: ")
						if x=="n" or x=="N":
							break
						set_input("")
				for na in nlff:
					print na
					f=open("list1.txt")
					while True:
						r=f.readline()
						print "Injecting the below payload: "
						print r
						driver = webdriver.Firefox()
						driver.get(site)
						time.sleep(10)
						try: 
							element = driver.find_element_by_name(na)
							element.send_keys(r)
							element.submit()
						except():
							d= "Parameter not tracable"
						driver.implicitly_wait(20)
						x = raw_input("Do u want to inject another payload??[y/n]: ")
						#x = tkinter.tkSimpleDialog.askstring
						if x=="n" or x=="N":
							set_input("")
							break
						set_input("")
		else:
			set_input("                               !!!URL-ERR0R!!!\n  \n                     Try adding 'https://' or 'http://' or 'www' in the url ")

main = Tk()
main.title("Penetration Testing Tools")
main.configure(background="#383a39")

Label(main, text = "Enter Url : ",font=('TIMES', 12,'bold'), bg="#383a39",fg="#d6d7d7").grid(row=1, column=1,sticky='E')
Label(main, text = "Result : ",font=('TIMES', 12,'bold'), bg="#383a39",fg="#d6d7d7").grid(row=2, column=1,sticky='En')

Te = Text(main,bg = "#e9eae9",fg = "black",font=('TIMES', 12), height=10, width=30)
Te.insert(END, "")
scroll = Scrollbar(main, command=Te.yview)
Te.configure(yscrollcommand=scroll.set)
scroll.config(bg="#383a39")


url = Entry(main,width=80,bg = "#e9eae9",fg = "black")
Te.grid(row=2, column=2,columnspan=10,sticky='Ensw')
scroll.grid(row=2,column=12,sticky='ens')
Te.config(state=DISABLED)

url.grid(row=1, column=2,columnspan=8,sticky='W')

main.rowconfigure(1, minsize=50)
main.rowconfigure(2, minsize=300)
main.rowconfigure(4, minsize=50)
main.rowconfigure(6, minsize=50)

Button(main, text='Quit',bg = "#e9eae9", fg="#000000",bd =0,font=('Times', 16,'bold'),highlightbackground="dark blue", command=main.destroy).grid(row=6,column=3,columnspan=3)
Button(main, text='Domain details', bg = "#e9eae9", fg="#000000",bd =0,font=('Times', 10,'bold'),highlightbackground="dark blue", command=domain).grid(row=4, column=2,pady=50,sticky='W')
Button(main, text='CMS for the URL',bg = "#e9eae9", fg="#000000",bd =0,font=('Times', 10,'bold'),highlightbackground="dark blue",  command=cms_f).grid(row=4, column=3,sticky='W')
Button(main, text='XSS Scanner 1', bg = "#e9eae9", fg="#000000",bd =0,font=('Times', 10,'bold'),highlightbackground="dark blue",  command=XSScanner1).grid(row=4, column=4,sticky='W')
Button(main, text='XSS Scanner 2', bg = "#e9eae9", fg="#000000",bd =0, font=('Times', 10,'bold'),highlightbackground="dark blue", command=Scanner2).grid(row=4, column=5,sticky='W')
quote="Spider URL\n(enter required domain)"
Button(main, text=quote, bg = "#e9eae9", fg="#000000",bd =0, font=('Times', 8,'bold'),highlightbackground="dark blue", command=spider).grid(row=4, column=6,sticky='W')
 
rClickbinder(main)
mainloop()
